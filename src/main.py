import pygame
import random
import time

# Inicializace Pygame
pygame.init()

# Přidání stavu hry
game_state = 'menu'
player_name = ''
input_active = False
input_text = ''

# Funkce pro vykreslení textu
def draw_text(text, font, color, surface, x, y):
    textobj = font.render(text, True, color)
    textrect = textobj.get_rect()
    textrect.topleft = (x, y)
    surface.blit(textobj, textrect)

# Funkce pro vykreslení tlačítka
def draw_button(text, font, color, surface, x, y, w, h, inactive_color, active_color):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(surface, active_color, (x, y, w, h))
        if click[0] == 1:
            return True
    else:
        pygame.draw.rect(surface, inactive_color, (x, y, w, h))
    draw_text(text, font, color, surface, x + 10, y + 10)
    return False

# Funkce pro ukládání a načítání skóre
def save_score(name, score):
    with open("leaderboard.txt", "a") as f:
        f.write(f"{name},{score}\n")

def load_leaderboard():
    leaderboard = []
    try:
        with open("leaderboard.txt", "r") as f:
            for line in f:
                name, score = line.strip().split(",")
                leaderboard.append((name, int(score)))
    except FileNotFoundError:
        pass
    leaderboard.sort(key=lambda x: x[1], reverse=True)
    return leaderboard[:10]

# Rozměry okna
screen_width = 800
screen_height = 600

# Barvy
black = (0, 0, 0)
white = (255, 255, 255)

# Vytvoření okna
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Space Invaders")

# Načtení obrázků
player_image = pygame.image.load('player.png')
enemy_image = pygame.image.load('enemy.png')
full_heart_image = pygame.image.load('full_heart.png')
empty_heart_image = pygame.image.load('empty_heart.png')
background_image = pygame.image.load('background.jpg')

# Načtení obrázků pro animaci výbuchu
explosion_images = [pygame.image.load(f'explosion{i}.png') for i in range(1, 4)]

# Požadované rozměry
player_width = 50
player_height = 50
enemy_width = 50
enemy_height = 50
bullet_width = 5
bullet_height = 10
heart_width = 30
heart_height = 30
explosion_width = 50
explosion_height = 50

# Změna velikosti obrázků
player_image = pygame.transform.scale(player_image, (player_width, player_height))
enemy_image = pygame.transform.scale(enemy_image, (enemy_width, enemy_height))
full_heart_image = pygame.transform.scale(full_heart_image, (heart_width, heart_height))
empty_heart_image = pygame.transform.scale(empty_heart_image, (heart_width, heart_height))
background_image = pygame.transform.scale(background_image, (screen_width, screen_height))  # Změna velikosti pozadí
explosion_images = [pygame.transform.scale(img, (explosion_width, explosion_height)) for img in explosion_images]

# Třída pro animaci výbuchu
class Explosion:
    def __init__(self, x, y):
        self.images = explosion_images
        self.index = 0
        self.x = x
        self.y = y
        self.time = pygame.time.get_ticks()
        self.interval = 50  # interval mezi snímky animace v ms
        self.done = False

    def update(self):
        if pygame.time.get_ticks() - self.time > self.interval:
            self.index += 1
            self.time = pygame.time.get_ticks()
            if self.index >= len(self.images):
                self.done = True

    def draw(self, surface):
        if not self.done:
            surface.blit(self.images[self.index], (self.x, self.y))

# Hráč
player_x = screen_width // 2 - player_width // 2
player_y = screen_height - player_height - 10
player_speed = 5

# Nepřátelé
enemy_speed_x = 2
enemy_speed_y = 1
enemy_rows = 6
enemy_cols = 10
enemy_padding = 10
enemies = []

# Vytvoření formace nepřátel
def create_enemies():
    global enemies
    enemies = []
    for row in range(enemy_rows):
        for col in range(enemy_cols):
            enemy_x = col * (enemy_width + enemy_padding) + enemy_padding
            enemy_y = row * (enemy_height + enemy_padding) + enemy_padding
            enemies.append([enemy_x, enemy_y])

create_enemies()

# Kulky hráče
bullet_speed = 7
bullets = []

# Kulky nepřátel
enemy_bullet_speed = 5
enemy_bullets = []

# Střelba hráče
rpm = 60  # Kolik střel za minutu
time_between_shots = 60 / rpm
last_shot_time = 0

# Životy hráče
lives = 3

# Skóre
score = 0
font = pygame.font.SysFont(None, 36)

# Směr pohybu nepřátel
enemy_direction = 1  # 1 pro doprava, -1 pro doleva

# Výbuchy
explosions = []

# Hlavní smyčka hry
running = True
clock = pygame.time.Clock()

def reset_game():
    global player_x, player_y, player_speed, enemies, bullets, enemy_bullets, lives, score, enemy_direction, explosions, last_shot_time
    player_x = screen_width // 2 - player_width // 2
    player_y = screen_height - player_height - 10
    player_speed = 5
    create_enemies()
    bullets = []
    enemy_bullets = []
    lives = 3
    score = 0
    enemy_direction = 1
    explosions = []
    last_shot_time = 0

def draw_menu():
    screen.fill(white)
    draw_button('Start', font, black, screen, 300, 200, 200, 50, (150, 150, 150), (100, 100, 100))
    draw_button('Leaderboard', font, black, screen, 300, 300, 200, 50, (150, 150, 150), (100, 100, 100))

def draw_enter_name():
    screen.fill(white)
    draw_text('Enter your name:', font, black, screen, 300, 200)
    pygame.draw.rect(screen, (200, 200, 200), (300, 250, 200, 50))
    if input_active:
        color = (100, 100, 100)
    else:
        color = (150, 150, 150)
    pygame.draw.rect(screen, color, (300, 250, 200, 50), 2)
    draw_text(input_text, font, black, screen, 310, 260)

def draw_playing():
    screen.blit(background_image, (0, 0)) 
    screen.blit(player_image, (player_x, player_y))
    
    for bullet in bullets:
        pygame.draw.rect(screen, black, (bullet[0], bullet[1], bullet_width, bullet_height))  
    
    for enemy in enemies:
        screen.blit(enemy_image, (enemy[0], enemy[1]))
    
    for bullet in enemy_bullets:
        pygame.draw.rect(screen, black, (bullet[0], bullet[1], bullet_width, bullet_height))  # Vykreslení černého čtverečku pro náboje nepřátel
    
    # Vykreslení výbuchů
    for explosion in explosions:
        explosion.draw(screen)
    
    # Vykreslení životů
    for i in range(3):
        if i < lives:
            screen.blit(full_heart_image, (10 + i * (heart_width + 10), screen_height - heart_height - 10))
        else:
            screen.blit(empty_heart_image, (10 + i * (heart_width + 10), screen_height - heart_height - 10))
    
    # Vykreslení skóre
    score_text = font.render(f'Score: {score}', True, black)
    screen.blit(score_text, (screen_width - score_text.get_width() - 10, screen_height - score_text.get_height() - 10))

def draw_leaderboard():
    screen.fill(white)
    draw_text("Leaderboard", font, black, screen, 300, 50)
    leaderboard = load_leaderboard()
    y = 100
    for i, (name, score) in enumerate(leaderboard):
        draw_text(f"{i+1}. {name} - {score}", font, black, screen, 300, y)
        y += 40
    if draw_button('Back', font, black, screen, 300, 500, 200, 50, (150, 150, 150), (100, 100, 100)):
        global game_state
        game_state = 'menu'

def draw_game_over():
    global game_state
    screen.fill(white)
    draw_text("Game Over", font, black, screen, 300, 200)
    if draw_button('Play Again', font, black, screen, 300, 300, 200, 50, (150, 150, 150), (100, 100, 100)):
        reset_game()
        game_state = 'playing'
    if draw_button('Return to Menu', font, black, screen, 300, 400, 200, 50, (150, 150, 150), (100, 100, 100)):
        game_state = 'menu'

# Hlavní smyčka hry
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        
        # Zpracování vstupu podle stavu hry
        if game_state == 'menu':
            if draw_button('Start', font, black, screen, 300, 200, 200, 50, (150, 150, 150), (100, 100, 100)):
                game_state = 'enter_name'
                input_active = True
                input_text = ''
            if draw_button('Leaderboard', font, black, screen, 300, 300, 200, 50, (150, 150, 150), (100, 100, 100)):
                game_state = 'leaderboard'

        elif game_state == 'enter_name':
            if event.type == pygame.KEYDOWN:
                if input_active:
                    if event.key == pygame.K_RETURN:
                        player_name = input_text
                        game_state = 'playing'
                    elif event.key == pygame.K_BACKSPACE:
                        input_text = input_text[:-1]
                    else:
                        input_text += event.unicode

    if game_state == 'menu':
        draw_menu()
    elif game_state == 'enter_name':
        draw_enter_name()
    elif game_state == 'leaderboard':
        draw_leaderboard()
    elif game_state == 'playing':
        keys = pygame.key.get_pressed()

        # Pohyb hráče
        if keys[pygame.K_LEFT] and player_x - player_speed > 0:
            player_x -= player_speed
        if keys[pygame.K_RIGHT] and player_x + player_speed < screen_width - player_width:
            player_x += player_speed
        
        # Střelba hráče
        current_time = time.time()
        if keys[pygame.K_SPACE] and current_time - last_shot_time >= time_between_shots:
            bullets.append([player_x + player_width // 2 - bullet_width // 2, player_y])
            last_shot_time = current_time
        
        # Pohyb kulek hráče
        for bullet in bullets[:]:
            bullet[1] -= bullet_speed
            if bullet[1] < 0:
                bullets.remove(bullet)
            else:
                # Kolize s nepřátelskými kulkami
                for enemy_bullet in enemy_bullets[:]:
                    if (bullet[0] < enemy_bullet[0] + bullet_width and
                        bullet[0] + bullet_width > enemy_bullet[0] and
                        bullet[1] < enemy_bullet[1] + bullet_height and
                        bullet[1] + bullet_height > enemy_bullet[1]):
                        bullets.remove(bullet)
                        enemy_bullets.remove(enemy_bullet)
                        break

                # Kolize s nepřáteli
                for enemy in enemies[:]:
                    if (bullet[0] < enemy[0] + enemy_width and
                        bullet[0] + bullet_width > enemy[0] and
                        bullet[1] < enemy[1] + enemy_height and
                        bullet[1] + bullet_height > enemy[1]):
                        bullets.remove(bullet)
                        enemies.remove(enemy)
                        explosions.append(Explosion(enemy[0], enemy[1]))  # Přidání animace výbuchu
                        score += 50  # Přičtení skóre za zabití nepřítele
                        break
        
        # Pohyb nepřátel
        move_down = False
        for enemy in enemies:
            enemy[0] += enemy_speed_x * enemy_direction
            if enemy[0] <= 0 or enemy[0] >= screen_width - enemy_width:
                move_down = True
        
        if move_down:
            enemy_direction *= -1
            for enemy in enemies:
                enemy[1] += enemy_speed_y
        
        # Náhodné střílení nepřátel
        if random.randint(0, 100) < 15:  # Pravděpodobnost střelby
            shooting_enemy = random.choice(enemies)
            enemy_bullets.append([shooting_enemy[0] + enemy_width // 2 - bullet_width // 2, shooting_enemy[1] + enemy_height])
        
        # Pohyb kulek nepřátel
        for bullet in enemy_bullets[:]:
            bullet[1] += enemy_bullet_speed
            if bullet[1] > screen_height:
                enemy_bullets.remove(bullet)
            else:
                # Kolize s hráčem
                if (bullet[0] < player_x + player_width and
                    bullet[0] + bullet_width > player_x and
                    bullet[1] < player_y + player_height and
                    bullet[1] + bullet_height > player_y):
                    enemy_bullets.remove(bullet)
                    lives -= 1
                    score -= 100  # Odečtení skóre při ztrátě života
                    if lives <= 0:
                        save_score(player_name, score)
                        game_state = 'game_over'
        
        # Kontrola, zda jsou všichni nepřátelé zničeni
        if not enemies:
            save_score(player_name, score)
            game_state = 'game_over'

        # Aktualizace a kreslení výbuchů
        for explosion in explosions[:]:
            explosion.update()
            if explosion.done:
                explosions.remove(explosion)

        draw_playing()

    elif game_state == 'game_over':
        draw_game_over()
    
    pygame.display.flip()
    clock.tick(60)

pygame.quit()
