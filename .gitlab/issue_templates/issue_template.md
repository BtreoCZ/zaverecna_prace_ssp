<!-- .gitlab/issue_templates/issue_template.md -->

## Issue Title

A brief and descriptive title of the issue

## Description

A detailed description of the issue, including what is happening, the expected results, and the actual results.

## Steps to Reproduce

1. Step one
2. Step two
3. ...
4. Final step

## Expected Behavior

Briefly describe what you expected to happen.

## Actual Behavior

Briefly describe what actually happened.

## Screenshots and Additional Evidence

Attach screenshots, logs, or other relevant material that helps to understand the issue better.

## Environment Information

- Application version:
- Operating system:
- Browser (if relevant):

## Additional Information

Add any additional information that might be useful in resolving the issue.

## Attachments

Attach any relevant files such as logs, configuration files, etc.

## Relevant Links

Links to relevant merge requests, commit messages, documentation, or other issues related to this problem.
